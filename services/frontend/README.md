# [Frontend](https://github.com/SciCatProject/frontend)

The SciCat frontend is the SciCat metadata catalogue web UI, built on top of the Angular framework. 

## Configuration options

The frontend configuration is set by the [config.json file](./config/config.json). For an extensive list of available options see [here](https://scicatproject.github.io/documentation/Development/v3.x/Configuration.html) in the SciCat frontend section.

## Defaul configuration

In the default configuration [config.json file](./config/config.json), the frontend is set to call the [backend container](../backend/) which is available on the `backend.localhost`.
