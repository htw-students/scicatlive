# [Searchapi](https://github.com/SciCatProject/panosc-search-api)

The SciCat seachAPI is the SciCat metadata catalogue standardised API for communication between SciCat and the PaN portal, built on top of the Loobpack framework. 

## Configuration options

The searchapi configuration is set by the [.env file](./config/.env). For an extensive list of available options see [here](https://github.com/SciCatProject/panosc-search-api).

## Defaul configuration

In the default configuration [.env file](./config/.env), the searchapi is set to call the [backend container](../backend/) which is available on `backend.localhost`.
